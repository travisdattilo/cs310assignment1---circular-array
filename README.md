## Project Overview

Implementation of an **ArrayLinearList**. A *list* is a sequence of values, which may include duplicates. The ordering of the items in the list is not specified but does matter. No insertion ever occurs at an arbitrary location. When an item is removed from the list, the ordering of the remaining elements in the list is unchanged. There are not any empty or unused cells bewteen the front and rear of the list. 

Inserting or removing an element at index[0] is a time consuming operation for arrays. Usually, to insert an element at index[0] when the array is not empty, then all elements must shift out of the way before insertion can occur. Similarly, if an element is removed at index[0] from a non-empty array, you must shift elements down to fill in the hole. This implementation can insert or remove elements from either end of the array in constant time with no shifting of elements necessary. A *circular array* strategy offers this capability. *Circular* is an abstraction, the underlying array does not form a circle but rather is a standard linear array.

The implementation abandons the notion that the first element in the list must be at index[0]. Instead, this implementation maintains a class level variable that holds the index of the "front" and "rear" of the list. The front and rear indices move independently, allowing insertion and deletion to happen without shifting anything. 

## Array Functions

- **addFirst:** Adds the Object obj to the beginning of list and returns true if the list is not full. Returns false and aborts the insertion if the list is full.
- **addLast:** Adds the Object obj to the end of list and returns true if the list is not full. Returns false and aborts the insertion if the list is full.
- **removeFirst:** Removes and returns the parameter object obj in first position in list if the list is not empty, null if the list is empty.
- **removeLast:** Removes and returns the parameter object obj in last position in list if the list is not empty, null if the list is empty.
- **remove:** Removes and returns the parameter object obj from the list if the list contains it, null otherwise. The ordering of the list is preserved. The list may contain duplicate elements. This method removes and returns the first matching element found when traversing the list from first position. Shift elements to fill in the slot where the deleted element was located.
- **peekFirst:** Returns the first element in the list, null if the list is empty. The list is not modified.
- **peekLast:** Returns the last element in the list, null if the list is empty. The list is not modified.
- **contains:** Returns true if the parameter object obj is in the list, false otherwise. The list is not modified.
- **find:** Returns the element matching obj if it is in the list, null otherwise. In the case of duplicates, this method returns the element closest to front. The list is not modified.
- **clear:** The list is returned to an empty state.
- **isEmpty:** Returns true if the list is empty, otherwise false
- **isFull:** Returns true if the list is full, otherwise false
- **size:** Returns the number of Objects currently in the list.
- **iterator:** Returns an Iterator of the values in the list, presented in the same order as the underlying order of the list. (front first, rear last)




