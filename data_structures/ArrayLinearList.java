/*  Travis Dattilo
    cssc0886
*/

package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

@SuppressWarnings({"unchecked"})	
public class ArrayLinearList<E> implements LinearListADT<E> {
    private int front, rear;
    private E[] list;
    
    public ArrayLinearList(int i) {
		E[] list = (E[])new Object[i];
		this.list = list;
		front = -1;
		rear = -1;
    }
    
    public ArrayLinearList(){
    	E[] list = (E[])new Object[DEFAULT_MAX_CAPACITY];
    	this.list = list;
    	front = -1;
    	rear = -1;
    }
    
//  Adds the Object obj to the beginning of list and returns true if the list is not full.
//  returns false and aborts the insertion if the list is full.
    public boolean addFirst(E obj){
    	if(isFull())
    	{
    		return false;
    	}
    	else if(isEmpty())
    	{
    		list[0] = obj;
    		front = rear = 0;
    	}
    	else if(front == 0)
    	{
    		list[list.length - 1] = obj;
    		front = list.length - 1;
    	}
    	else//rear is not at edge of wrapping edge case
    	{
    		list[front - 1] = obj;
    		front--;
    	}
    	return true; 
    }
    
//  Adds the Object obj to the end of list and returns true if the list is not full.
//  returns false and aborts the insertion if the list is full..  
    public boolean addLast(E obj){
    	if(isFull())
    	{
    		return false;
    	}
    	else if(isEmpty())
    	{
    		list[0] = obj;
    		front = rear = 0;
    	}
    	else if(rear == list.length - 1)
    	{
    		list[0] = obj;
    		rear = 0;
    	}
    	else//rear is not at edge of wrapping edge case
    	{
    		list[rear + 1] = obj;
    		rear++;
    	}
    	return true; 
    }
    
//  Removes and returns the parameter object obj in first position in list if the list is not empty,  
//  null if the list is empty. 
    public E removeFirst(){
    	if(!isEmpty())
    		return remove(list[front]);
    	return null;
    }
    
//  Removes and returns the parameter object obj in last position in list if the list is not empty, 
//  null if the list is empty. 
    public E removeLast(){
    	if(!isEmpty()) 
    		return remove(list[rear]);
    	return null;
    }
    
//  Removes and returns the parameter object obj from the list if the list contains it, null otherwise.
//  The ordering of the list is preserved.  The list may contain duplicate elements.  This method
//  removes and returns the first matching element found when traversing the list from first position.
//  Note that you may have to shift elements to fill in the slot where the deleted element was located.
    public E remove(E obj){
    	int currentSize = size();
    	if(currentSize == 0) return null;
    	if(currentSize == 1)	
    	{
    		if(((Comparable<E>)obj).compareTo(list[front])==0)	//Comparing equality to the only element in the list		
    		{
    			E temp = list[front];									
    			front = rear = -1;	//Sets front and rear so there's nothing in the list
    			return temp;
    		}
    		return null;	
    	}
    	if(rear < front)
    	{
    		for(int i = 0;i <= rear; i++)	//finding the index of the item to remove between index 0 to index of rear
    		{
    			if(((Comparable<E>)obj).compareTo(list[i])==0)
    			{
    				E tmp = list[i];
    				removeAt(i); 
    				return tmp;
    			}
    		}
    		for(int i = front; i < list.length; i++)	//finding the index of the item to remove between index of front to end of the list
    		{
    			if(((Comparable<E>)obj).compareTo(list[i])==0)
    			{
    				E tmp = list[i];
    				removeAt(i); 
    				return tmp;
    			}
    		}
    	}
    	else	//(rear > front)
    	{
    		for(int i = front; i <= rear; i++)	//finding the index of the item to remove between front to rear						
    		{
    			if(((Comparable<E>)obj).compareTo(list[i])==0) 
    			{
    				E tmp = list[i];
    				removeAt(i); 
    				return tmp;
    			}
    		}
    	}
    	return null;
    		
    }
    
    private void removeAt(int index)	//This helper method for remove for removing at specific indicies
    {
    	if(front < rear)
    	{
    		int frontCount = index - front;
    		int rearCount = rear - index;
    		if(frontCount < rearCount)
    		{
    			//shift front
    			for(int i = index; i > front; i--)	//Looping through the list. Shifting over or overwriting the element being removed
    				list[i] = list[i-1];
    			front++;
    		}
    		else
    		{
    			//shift rear
    			for(int i = index; i < rear; i++)	//Looping through the list. Shifting over or overwriting the element being removed
        			list[i] = list[i+1];
        		rear--;	
    		}					
    	}
    	else	//(front > rear)
    	{
    		if(index <= rear)
    		{
    			//shift rear
    			for(int i = index; i < rear; i++)	//Shift the rear left to overwrite the removed value
        			list[i] = list[i+1];	
        		if(--rear == 0)				//Before we decrement rear into the correct place (overwriting the removed element) See if it's at index 0
        		{
        			rear = list.length - 1;		//If it is, then put it at the last index in the array.
        		}
    		}
    		else
    		{
    			//shift front
    			for(int i = index; i > front; i--)	//Shift the front right to overwrite the removed value
    				list[i] = list[i-1];
    			if(++front == list.length)		//Before we increment front into the correct place (overwriting the removed element) See if it's at last index 
    			{
    				front = 0;			//If it is, then put it at index 0 of the array
    			}
    		}
    	}
    	
    }
    
//  Returns the first element in the list, null if the list is empty.
//  The list is not modified.
    public E peekFirst(){
    	if(isEmpty())
    		return null;
    	else
    		return list[front];
    }
    
//  Returns the last element in the list, null if the list is empty.
//  The list is not modified.
    public E peekLast(){
    	if(isEmpty())
    		return null;
    	else
    		return list[rear]; 
    }

//  Returns true if the parameter object obj is in the list, false otherwise.
//  The list is not modified.
    public boolean contains(E obj){
    	return find(obj) != null;
    }
    
//  Returns the element matching obj if it is in the list, null otherwise.
//  In the case of duplicates, this method returns the element closest to front.
//  The list is not modified.
    public E find(E obj){
    	for(E temp: this)	//uses iterator
    	{
    		if(((Comparable<E>)obj).compareTo(temp)==0)
    		{
    		return temp;
    		}
    	}
    	return null;
    }

//  The list is returned to an empty state.
    public void clear(){
    	front = rear = -1;
    }

//  Returns true if the list is empty, otherwise false
    public boolean isEmpty(){
    	return size() == 0;
    }
    
//  Returns true if the list is full, otherwise false
    public boolean isFull(){
    	return size()== list.length; 
    }

//  Returns the number of Objects currently in the list.
    public int size(){
    	if(front < 0 || rear < 0) return 0;				//No objects in the list
    	if(front == rear) return 1;					//One object in the list
    	if(rear < front)return ((list.length - front) + (rear + 1));	//More than one object, rear is behind the front
    	return rear - front + 1; 					//More than one object, front is behind the rear   		
    }
    
//  Returns an Iterator of the values in the list, presented in
//  the same order as the underlying order of the list. (front first, rear last)
    public Iterator<E> iterator(){
    	return new IteratorHelper();
    }
    class IteratorHelper implements Iterator <E> {			//This Iterator code is from the supplementary material
    	private int count, index;
    	public IteratorHelper(){
    		index = front;
    		count = 0;
    	}
    	public boolean hasNext(){
    		return count != size();
    	}
    	public E next(){
    		
    		if(!hasNext()) throw new NoSuchElementException();
    			E temp = list[index++];
    		if(index == list.length)index = 0;
    			count++;
    		return temp;
    	}
    	public void remove(){
    		throw new UnsupportedOperationException();
    	}
    }
}
